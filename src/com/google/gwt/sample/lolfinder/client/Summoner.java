package com.google.gwt.sample.lolfinder.client;

public class Summoner {
	private String ID;
	private String name;
	private int secs;
	
	public Summoner(String ID, String name, String secs)
	{
		this.ID = ID;
		this.name = name;
		this.secs = Integer.parseInt(secs);
	}
	
	public String getID() {
		return this.ID;
	}
	
	public void setID(String ID) {
		this.ID = ID;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getSecs() {
		return this.secs;
	}
	
	public void setSecs(int secs) {
		this.secs = secs;
	}
}
