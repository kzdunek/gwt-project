package com.google.gwt.sample.lolfinder.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoLfinder implements EntryPoint {

	private VerticalPanel mainPanel = new VerticalPanel();
	  private FlexTable flexTable = new FlexTable();
	  private HorizontalPanel addPanel = new HorizontalPanel();
	  private TextBox newTextBox = new TextBox();
	  private Button addButton = new Button("Add");
	  private Label lastUpdatedLabel = new Label();
	  private static List<String> summoners = new ArrayList<>();
	
	private final LolfinderServiceAsync greetingService = GWT
			.create(LolfinderService.class);

	public void onModuleLoad() {
		
		flexTable.setText(0, 0, "ID");
		flexTable.setText(0, 1, "Nazwa gracza");
		flexTable.setText(0, 2, "Ostatnio widziany");
		flexTable.setText(0, 3, "Usun");
		
		flexTable.getRowFormatter().addStyleName(0, "listHeader");
		flexTable.addStyleName("playerList");
		flexTable.getCellFormatter().addStyleName(0, 1, "listNumericColumn");
		flexTable.getCellFormatter().addStyleName(0, 2, "listNumericColumn");
		flexTable.getCellFormatter().addStyleName(0, 3, "listRemoveColumn");
		
		addPanel.add(newTextBox);
		addPanel.add(addButton);
		addPanel.addStyleName("addPanel");
		
		mainPanel.add(flexTable);
		mainPanel.add(addPanel);
		lastUpdatedLabel.setStyleName("infoText");
		mainPanel.add(lastUpdatedLabel);
		
		RootPanel.get("mainList").add(mainPanel);
		RootPanel.get("mainList").addStyleName("mainList");
		
		newTextBox.setFocus(true);
		
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event)
			{
				addSummoner();
			}
		});
		
		newTextBox.addKeyDownHandler(new KeyDownHandler() {
		      public void onKeyDown(KeyDownEvent event) {
		        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
		        	addSummoner();
		        }
		      }
		    });

	      Timer refreshTimer = new Timer() {
	        @Override
	        public void run() {
	          refreshList();
	        }
	      };
	      refreshTimer.scheduleRepeating(6000);
	}
	
	void addSummoner()
	{
		final String symbol = newTextBox.getText().trim();
		newTextBox.setFocus(true);
	    
	    if (!symbol.matches("^[0-9A-Za-z&#92;&#92;. ]{3,20}$")) {
	      newTextBox.selectAll();
	      return;
	    }

	    newTextBox.setText("");

	    if(summoners.contains(symbol)) return;
	    int row = flexTable.getRowCount();
	    summoners.add(symbol);
	    flexTable.setText(row, 1, symbol);
	    Button removeButton = new Button("x");
	    removeButton.addClickHandler(new ClickHandler() {
	      public void onClick(ClickEvent event) {
	        int removedIndex = summoners.indexOf(symbol);
	        summoners.remove(removedIndex);
	        flexTable.removeRow(removedIndex + 1);
	      }
	    });
	    flexTable.setWidget(row, 3, removeButton);
	    flexTable.getCellFormatter().addStyleName(row, 1, "listNumericColumn");
		flexTable.getCellFormatter().addStyleName(row, 2, "listNumericColumn");
		flexTable.getCellFormatter().addStyleName(row, 3, "listRemoveColumn");
	    refreshList();
	}
	
	void refreshList()
	{
		for(int i=0;i<summoners.size();i++) {

			greetingService.greetServer(summoners.get(i),
					new AsyncCallback<String>() {
						public void onFailure(Throwable caught) {
						}

						public void onSuccess(String result) {
							String[] spl = result.split(":");
							int row = summoners.indexOf(spl[1]) + 1;

						     if(row == 0) {
						    	 return;	
						     }

						     flexTable.setText(row, 0, spl[0]);
						     flexTable.setText(row, 1, spl[1]);
						     flexTable.setText(row, 2, spl[2]);					
						}
					});
		}
		DateTimeFormat dateFormat = DateTimeFormat.getFormat(
		        DateTimeFormat.PredefinedFormat.DATE_TIME_MEDIUM);
		      lastUpdatedLabel.setText("Ostatnia aktualizacja: " 
		        + dateFormat.format(new Date()));
	}
	
}
