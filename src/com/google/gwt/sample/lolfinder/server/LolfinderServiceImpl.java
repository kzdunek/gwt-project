package com.google.gwt.sample.lolfinder.server;

import java.io.IOException;
import java.util.Date;

import com.google.gwt.sample.lolfinder.client.LolfinderService;
import com.google.gwt.sample.lolfinder.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("serial")
public class LolfinderServiceImpl extends RemoteServiceServlet implements
		LolfinderService {

	public String greetServer(String input) throws IllegalArgumentException {
		if (!FieldVerifier.isValidName(input)) {
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);
		
		JSONObject json = null;
		try {
			String s = "https://eune.api.pvp.net/api/lol/eune/v1.4/summoner/by-name/" + 
		                input.toLowerCase() + "?api_key=ff6ad3e6-2832-4006-ac89-6cb6a78b28d1";
			json = JsonReader.readJsonFromUrl(s);
		} catch (JSONException | IOException e) {
			return "--------" + input + ":brak gracza";
		}
		
		try {
			Object k = json.get(input.toLowerCase());
			String d = String.valueOf((((JSONObject)k).get("revisionDate")).toString());
			
			Date now = new Date();

			double dif = now.getTime() - Double.parseDouble(d);
			
			int secs = (int) (dif / 1000.0);
			int dni = 0;
			
			int h = secs/3600;
			secs -= h*3600;
			int m = secs/60;
			secs-=m*60;
			int s = secs/60;
			secs-=s*60;
			
			if(h > 24) {
				dni = h /24;
				h -= dni * 24;
			}
			
			String gotowe;
			if(dni > 0) {
				gotowe = String.valueOf(dni) + " dni " + String.valueOf(h) + " godzin " + String.valueOf(m) + " minut " + String.valueOf(secs) + " sekund";
			} else {
				gotowe = String.valueOf(h) + " godzin " + String.valueOf(m) + " minut " + String.valueOf(secs) + " sekund";
			}
			
			String l = ((JSONObject)k).get("id") + ":" + input + ":" + gotowe;
			return l;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}
}
